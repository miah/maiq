require 'maiq/version'

module Maiq
  class Quotes
    def quotes
      [
      # Liches
      "You wish to become a lich? It's very easy, my friend. Simply find " \
      'the heart of a lich, combine it with the tongue of a dragon, and cook ' \
      'it with the flesh of a well-ridden horse. This combination is certain ' \
      'to make you undead.',

      # Dragons
      "Dragons? Oh, they're everywhere! You must fly very high to see most of " \
      'them, though. The ones nearer the ground are very hard to see, being ' \
      'invisible.',

      # Emperor Crabs
      "M'aiq sees lots of them in the ocean. M'aiq knows you'll see one too " \
      'if you swim far enough.',

      # Horses
      "Horses.... Oh, M'aiq loves horses! Especially with good cream sauce.",

      # Horses
      'You would wish to ride upon a beast? There is a way... Go to one of ' \
      'the many silt-strider ports and pay your fee! You wish one for personal ' \
      'use? Bah! Walk if you must; run if you are chased!',

      # Corpses
      "Moving corpses? This sounds frightening to M'aiq. The undead are " \
      'nothing to be toyed with.',

      # Multiplayer
      "M'aiq does not know this word. You wish others to help you in your " \
      'quest? Coward! If you must, search for the Argonian Im-Leet, or ' \
      'perhaps the big Nord, Rolf the Uber. They will certainly wish to join ' \
      'you.',

      # Dwemer
      "There is no mystery. M'aiq knows all. The dwarves were here, and now " \
      'they are not! They were very short folk... Or perhaps they were not. ' \
      "It all depends on your perspective. I'm sure they thought they were " \
      'about the right height.',

      # Naked Liches
      "A horrible thing indeed. If you see one, let M'aiq know. M'aiq wants " \
      'to make sure to look in the other direction.',

      # Nudity
      'Ahh... The beauty of the naked form. These Dunmer are rather prudish, ' \
      'are they not? Of course, there is an island you can reach filled with ' \
      'wonderful, naked, glistening bodies. It only appears when the moons ' \
      "are full, the rain falls, the seas run red, and it's M'aiq's birthday.",

      # Climbing
      "Climbing ropes that hang is too difficult. M'aiq prefers to climb the " \
      'ones that are tied horizontally.',

      # Shrine of Boethiah
      'You seek the shrine that is no longer there? An interesting concept. ' \
      'Look to the seas to the West. There lies what was once the shrine. ' \
      'Take a deep breath and begin your search.',

      # Mudcrab Merchant
      "M'aiq has heard of this. They've got all the money. Mudcrabs taking " \
      'over everything. They already run Pelagiad.',

      # Weresharks
      'I have only met one, but he was afraid of the water.',

      # Unspecific quotes
      'Feet are for walking. Hands are for hitting. Or shaking. Or waving. ' \
      'Sometimes for clapping.',

      'I do not wish to fight on horseback. It is a good way to ruin a ' \
      'perfectly good horse... which is, to say, a perfectly good dinner.',

      "I don't know why one would want to destroy a building. It takes time " \
      'to make it. Much time.',

      "I have seen dragons. Perhaps you will see a dragon. I won't say where " \
      'I saw one. Perhaps I did not.',

      "It is good the people wear clothing. M'aiq wears clothing. Who would " \
      "want to see M'aiq naked? Sick, sick people. Very sad.",

      'Levitation is for fools. Why would we want to levitate? Once you are up ' \
      'high, there is nowhere to go but down.',

      "M'aiq believes the children are our future. But he doesn't want them " \
      'ruining all of our fun.',

      "M'aiq is glad he has a compass. Makes it easy to find things. Much " \
      'better than wandering around like a fool.',

      "M'aiq knows much, tells some. M'aiq knows many things others do not.",

      "M'aiq longs for a Colovian Fur Helm. Practical, yet stylish. M'aiq is " \
      'very sad he does not have one.',

      "M'aiq prefers to adventure alone. Others just get in the way. And they " \
      'talk, talk, talk.',

      "M'aiq thinks his people are beautiful. The Argonian people are " \
      'beautiful as well. They look better than ever before.',

      "M'aiq wishes he had a stick made out of fishies to give to you. " \
      'Sadly, he does not.',

      "People always enjoy a good fable. M'aiq has yet to find one, though. " \
      'Perhaps one day.',

      'So much easier to get around these days. Not like the old days. Too ' \
      "much walking. Of course, nothing stops M'aiq from walking when he wants.",

      'Some people want special bows that take too long to load and need ' \
      "special arrows called bolts. M'aiq thinks they are idiots.",

      "Some people wish to throw their weapons. That seems foolish to M'aiq. " \
      'If you hold your weapon, you only need one.',

      'Werewolves? Where? Wolves? Men that are wolves? Many wolves. ' \
      "Everywhere. Many men. That is enough for M'aiq.",

      'Why would one want to swing a staff? A mace hurts more. Or a sword. ' \
      "Can't shoot a fireball from a sword, though.",

      "M'aiq's father was also called M'aiq. As was M'aiq's father's father. " \
      'At least, that is what his father said. But then again, you can never ' \
      'trust a liar.',

      "M'aiq wishes you well.",

      "M'aiq knows much, and tells some. M'aiq knows many things others do not.",

      "M'aiq carries two weapons, to be safe. What if one breaks? That would " \
      'be most unlucky.',

      "M'aiq is always in search of calipers, yet he finds none. Where could " \
      'they have gone?',

      "M'aiq hears many stories of war... yet few of them are true.",

      "How does anyone know there was a city of Winterhold? M'aiq did not see " \
      'it with his eyes. Did you?',

      "Too much magic can be dangerous. M'aiq once had two spells and burned " \
      'his sweetroll.',

      'What does this mean, to combine magic? Magic plus magic is still magic.',

      "Some say Alduin is Akatosh, some say M'aiq is a Liar. Don't you " \
      'believe either of those things.',

      "It does not matter to M'aiq how strong or smart one is. It only " \
      'matters what one can do.',

      'Dragons were never gone. They were just invisible and very, very quiet.',

      'Werebears? Where? Bears? Men that are bears?',

      "Much snow in Skyrim. Enough snow. M'aiq does not want it anymore.",

      "Snow falls. Why worry where it goes? M'aiq thinks the snowflakes " \
      'are pretty.',

      "M'aiq once walked to High Hrothgar. So many steps, he lost count.",

      "Once M'aiq got into trouble in Riften, and fled to Windhelm. It " \
      'is good that nobody there cared.',

      "M'aiq can travel fast across the land. Some lazy types take carriages. " \
      "It is all the same to M'aiq.",

      "M'aiq does not understand what is so impressive about shouting. M'aiq " \
      'can shout whenever he wants.',

      "M'aiq saw a mudcrab the other day. Horrible creatures.",

      "M'aiq loves the people of Skyrim. Many interesting things they say to " \
      'each other.',

      "Nords are so serious about beards. So many beards. M'aiq thinks they " \
      'wish they had glorious manes like Khajiit.',

      "M'aiq does not remember his childhood. Perhaps he never had one.",

      "M'aiq is very practical. He has no need for mysticism.",

      "Nord's armor has lots of fur. This sometimes makes M'aiq nervous.",

      "M'aiq was soul trapped once. Not very pleasant. You should think " \
      'about that once in a while.',

      'Something strange happens to Khajiit when they arrive in Skyrim.',

      "M'aiq has heard the people in Skyrim are better-looking than the " \
      'ones in Cyrodiil. He has no opinion on the matter. All people are ' \
      'beautiful to him.',

      'Why do soldiers bother with target practice? One learns best by ' \
      'hitting real people.',

      "M'aiq knows why Falmer are blind. It has nothing to do with the " \
      'Dwemer disappearing. Really.',

      "M'aiq has heard it is dangerous to be your friend.",

      'The people of Skyrim are more open-minded about certain things than ' \
      'people in other places.',

      "Some like taking friends on adventures. M'aiq thinks being alone is " \
      'better. Less arguing about splitting treasure.',

      "Don't try blocking if you have two weapons. You will only get " \
      'confused. Much better to hit twice anyway.',

      "M'aiq knows many things, no?",

      'Skyrim was once the land of many butterflies. Now, not so much.',

      "M'aiq is tired now. Go bother somebody else.",

      "M'aiq is done talking.",

      "M'aiq tried to swim out to sea, but had to turn back. Slaughterfish. " \
      'Always the slaughterfish.',

      "\"Come no closer\" said the ghost, so M'aiq did not. But she kept " \
      'saying it, wherever he went.',

      "Wood Elves aren't made of wood. Sea Elves aren't made of water. M'aiq " \
      'still wonders about High Elves.',

      "M'aiq wonders why merchants boast of spacious bags, but never how much "\
      'weight they carry.',

      "M'aiq speaks with many in his travels. After a time, they seem to " \
      'repeat themselves. Strange.',

      "M'aiq despises bats. Tiny, winged skeevers. Disgusting!",

      "M'aiq heard an old-time speak of lost mittens. Keep looking, M'aiq. " \
      'Mittens will turn up somewhere.',

      "M'aiq can't tell Spider Cultists apart from the usual riffraff. " \
      'Perhaps they need matching uniforms?',

      'Come back later. We can talk some more!',

      "Have you seen dragons? No? M'aiq thinks they must be hiding...for now.",

      "M'aiq doesn't care if the tunnels beneath Wayrest are a feat of " \
      'engineering. The smell is horrific!',

      "M'aiq paid a merchant to ship his things to Stormhaven. Half went " \
      'to Stormhold, the rest to Haven. Stupid merchant.'
      ].freeze
    end

    def quote
      self.quotes.sample
    end

    def greetings
      "Greetings! M'aiq knows many things. What is your interest? You seek " \
      "knowledge. M'aiq has much. Some of it verified by actual facts!" \
      "(Get a quote with !maiq)"
    end
  end
end
